<?php
include "includes/db.php";
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css"> <!-- подключаем файл стилей -->
    <link rel="stylesheet" href="css/test.css"> <!-- подключаем файл стилей -->
    <title>Document</title>
</head>

<body background="images/bg-main.jpg">
    <header id="header" class="header">
        <div class="container">
            <div class="navigation">
                <a href="index.php"><img src="images/logo.png" alt="Webant" class="logo-Webant"></a>
                <ul class="menu">
                    <li>
                        <a href="index.php" class="main">
                            Главная
                        </a>
                    </li>
                    <li>
                        <a href="portfolio.php" class="contacts">
                            Портфолио
                        </a>
                    </li>
                    <li>
                        <a href="services.php" class="faculties">
                            Услуги и стоимость
                        </a>
                    </li>
                    <li>
                        <a href="events.php" id="glave" class="rules">
                            Мероприятия
                        </a>
                    </li>
                    <li>
                        <a href="/includes/auth/register.php" class="rules">
                            Регистрация
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <section id="content" class="content">
        <div class="container">
            <div class="contacts">
                <h2 id="glawe-title">Наши Мероприятия:</h2>
                <p>Мы регулярно проводим разные мероприятия. Митапы, конференции, интенсивы, ворк-шопы и стади-джемы.</p>
                <div class="project">
                    <div class="card" style="border: 3px solid transparent; border-image: 10 repeating-linear-gradient(90deg, #f598a8, #f6edb2)">
                        <?php
                        $events = mysqli_query($connection, "SELECT * FROM `events`");
                        ?>
                        <?php
                        while ($event = mysqli_fetch_assoc($events)) {
                        ?>
                            <h1><?php echo $event['title']; ?><style>
                                    .card h1 {
                                        display: inline;
                                        border-bottom: 3px solid #f9dd94
                                    }

                                    ;
                                </style>
                            </h1>
                            <p><?php echo $event['text']; ?></p>
                            <img src="images/events/<?php echo $event['pictures']; ?>" style="width:100%; border: 2px solid #B0C4DE;">
                            <p><button onclick="document.location='<?php echo $event['url']; ?>'">Перейти...</button></p>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
    </section>
    <footer id="footer" class="footer">
        <div class="container">
            <div class="adress">
                <h3>Адрес</h3>
                <p>
                    344000, г. <br> Ростов-на-Дону, пер.Кировский 35/113
                </p>
            </div>
            <div class="social">
                <h3>Соцсети</h3>
                <div class="soc-links">
                    <a href="#">
                        <img src="images/1.svg" alt="Фейсбук">
                    </a>
                    <a href="#">
                        <img src="images/2.svg" alt="Вконтакте">
                    </a>
                    <a href="#">
                        <img src="images/3.svg" alt="Твиттер">
                    </a>
                    <a href="#">
                        <img src="images/4.svg" alt="Инстаграм">
                    </a>
                    <a href="#">
                        <img src="images/5.svg" alt="Ютуб">
                    </a>
                    <a href="#">
                        <img src="images/6.svg" alt="Телеграм">
                    </a>
                </div>
            </div>
            <div class="email">
                <h3>Почта</h3>
                <a href="mailto:v@webant.ru">
                    v@webant.ru
                </a>
            </div>
        </div>
    </footer>
</body>

</html>