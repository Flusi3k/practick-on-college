-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 07 2022 г., 15:23
-- Версия сервера: 5.6.51-log
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `projectdb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `cat` varchar(55) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `pictures` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `cat`, `title`, `text`, `pictures`, `url`) VALUES
(1, 'Приложение', 'Мобильное приложение Тизер.Авто', 'Автоматизированный инструмент для дистанционных продаж автомобилей и нормочасов с помощью видео. Возможности сервиса: режиссерские сценарии видеошаблонов, автовыгрузка практически на любую платформу и в любую соц. сеть дилера, интеграция фидов с классифай\r\n', 'project-1.png', 'https://tizerauto.ru'),
(2, 'Приложение', 'Мобильное приложение Autoexpert', 'Кроссплатформенное мобильное приложение для создания акта осмотра автомобилей. Приложение позволяет пошагово заполнить данные об осматриваемом автомобиле, включая фотографии внешнего вида и интерьера.\r\n', 'project-2.png', 'https://webant.ru/projects/82'),
(3, 'Сервис', 'Сервис для проведения турниров GlobalRank', 'Сервис для организации и проведения спортивных турниров.\r\n', 'project-3.png', 'https://globalrank.ru'),
(4, 'Сайт', 'Викторина \"Разная, но единая\"', 'Сайт-викторина, приуроченный ко Дню Конституции РФ.', 'project-4.png', 'https://деньроссии.рф'),
(5, 'Приложение', 'Мобильное приложение Medx.pro', 'Мобильное кроссплатформенное приложение для медицинского образовательного сервиса Medx.pro.\r\n', 'project-5.png', 'https://medx.pro'),
(6, 'Приложение', 'Мобильное приложение Короче, дилер', 'Кроссплатформенное приложение для покупки и чтения книг по теме автобизнеса с поддержкой аудиокниг.\r\n', 'project-6.png', 'https://auto-pub.ru'),
(7, 'Приложение', 'Мобильное приложение ТопФан', 'Кроссплатформенное приложение для футбольных фанатов.', 'project-7.png', 'https://topfan.app'),
(8, 'Сайт', 'Сайт для Обкома Ростовской области', 'Сайт для  Ростовской областной организации Профсоюза работников народного образования и науки РФ.', 'project-8.png', 'http://www.obkomprof.ru'),
(9, 'Приложение', 'Мобильное приложение с нейронной сетью Corteva Expert', 'Сканер для распознавания сорняков, вредителей и болезней на сельскохозяйственных культурах', 'project-9.png', 'https://www.corteva.ru'),
(10, 'Сервис', 'Сервис Рейтинг ЮФУ', 'Сервис для верификации достижений студентов Южного федерального университета, проведения конкурсов портфолио студентов.', 'project-10.png', 'https://sfedu.ru'),
(11, 'Приложение', 'Мобильное приложение WIS Синхронный перевод', 'Сервис, который предназначен для организации мероприятий с иностранными спикерами, позволяя вам слушать перевод доклада в мобильном телефоне.', 'project-11.png', 'https://wise.com/ru/'),
(12, 'Сайт', 'Лига изобретателей IMAKE', 'Онлайн-викторина для школьников, направленная на развитие научно-технического потенциала регионов присутствия компании «Норильский никель».', 'project-12.png', 'https://ligaimake.ru'),
(13, 'Приложение', 'Мобильное приложение Доступная еда', 'Адаптация меню заведений общественного питания для незрячих и слабовидящих людей.', 'project-13.png', 'https://www.ntv.ru/novosti/2657602/'),
(14, 'Сайт', 'Сайт Квиз, плиз', 'Сервис для организации интеллектуально-развлекательных квизов в барах и на онлайн платформе.', 'project-14.png', 'https://quizplease.ru'),
(15, 'Приложение', 'Мобильное приложение AVTOTO', 'Мобильное iOS приложение для магазина запчастей для иномарок. В приложении можно найти деталь по VIN-номеру, добавить в корзину, оформить доставку.', 'project-15.png', 'https://www.avtoto.ru'),
(16, 'Приложение', 'Мобильное приложение для конференции DevFest', 'Приложение для просмотра расписания и новостей ежегодной конференции GDG SouthDevfest.', 'project-16.png', 'https://webant.ru/projects/79'),
(17, 'Приложение', 'Мобильное приложение Промполы', 'Мобильное приложение с админ. панелью. Приложение для лакокрасочной компании. Сервис направлен на организацию и оптимизацию взаимодействия между клиентами компании, а также получение клиентами актуальной и полезной информации о продукции и способах ее применения.', 'project-17.png', 'https://linolit.ru'),
(18, 'Сервис', 'Pinex', 'Криптовалютная биржа. Параметры биржи представлены такими разделами, как обменник, маржинальная торговля, также есть market, который представлен 4 видами криптовалюты: можно торговать в BTC, ETH, SUQA и др. Изменения за сутки показываются в графике маркета. Также указывается цена на данный промежуток времени, вы можете увидеть изменения за 24 часа. Помимо этого на бирже есть возможность составить открытые ордера и у всех пользователей есть возможность поставить стоп лимит. В сервисе доступен функционал графической аналитики (график), который позволяет выставлять увеличение графика криптовалюты от 6 часов до полного срока. Pinex позволяет ознакомиться с графиком предложений и покупок.', 'project-18.png', 'https://www.pinaxart.ru'),
(19, 'Сервис', 'Профтест', 'Профориентационный тест для молодежного фестиваля. Сервис анализирует сильные и слабые стороны ребенка, на основе анализа выводит диаграмму способностей и рекомендуемые профессии.', 'project-19.png', 'https://careertest.ru/tests/'),
(20, 'Приложение', 'Мобильное приложение Дают-бери', 'Мобильное приложение и админ. панель. Приложение позволяет получать различные бонусы (билеты на общественный транспорт, обеды в кафе, оплата штрафов ГИБДД и т.д.) за просмотры рекламных видеороликов. На сервисе есть возможность таргетировать рекламу в зависимости от пола, возраста, социального положения, интересов, геолокации аудитории.', 'project-20.png', 'http://dayut-beri.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `categories_id` int(11) NOT NULL,
  `categories` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`categories_id`, `categories`) VALUES
(1, 'Сайт'),
(2, 'Приложение'),
(3, 'Сервис');

-- --------------------------------------------------------

--
-- Структура таблицы `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `pictures` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `events`
--

INSERT INTO `events` (`id`, `title`, `text`, `pictures`, `url`) VALUES
(1, 'South Dev Fest', 'South DevFest - часть глобальной серии мероприятий, проводимых по всему миру сообществами Google Developer Group при поддержке компании Google. Многие члены нашей команды являются лидерами GDG RND сообщества. Мы провели уже шесть конференция DevFest в Ростове-на-Дону. В последний год её посетили более 800 человек.', 'event-1.jpg', 'https://webant.ru/events/6'),
(2, 'Code in the dark', 'Сложно придумать более неформальное IT-соревнование. Шот крепкого алкоголя и 15 минут на выполнение задания.', 'event-2.jpg', 'https://webant.ru/events/5'),
(3, 'Women Techmakers RND', 'WTM - это международная программа Google для женщин, работающих или интересующихся сферой IT. Цель движения - обмен опытом, развитие и расширение возможностей женщин в отрасли.', 'event-3.jpg', 'https://webant.ru/events/2');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categories_id`);

--
-- Индексы таблицы `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `categories_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
