<?php
include "includes/db.php";
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css"> <!-- подключаем файл стилей -->
    <link rel="stylesheet" href="css/test.css"> <!-- подключаем файл стилей -->
    <title>Document</title>
</head>

<body background="images/bg-main.jpg">
    <header id="header" class="header">
        <div class="container">
            <div class="navigation">
                <a href="index.php"><img src="images/logo.png" alt="Webant" class="logo-Webant"></a>
                <ul class="menu">
                    <li>
                        <a href="index.php" class="main">
                            Главная
                        </a>
                    </li>
                    <li>
                        <a href="portfolio.php" class="contacts">
                            Портфолио
                        </a>
                    </li>
                    <li>
                        <a href="services.php" id="glave" class="faculties">
                            Услуги и стоимость
                        </a>
                    </li>
                    <li>
                        <a href="events.php" class="rules">
                            Мероприятия
                        </a>
                    </li>
                    <li>
                        <a href="/includes/auth/register.php" class="rules">
                            Регистрация
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <section id="content" class="content">
        <div class="container">
            <h2 id="glawe-title">Наши Услуги:</h2>
            <div class="wrapper" style="border: 3px solid transparent; border-image: 10 repeating-linear-gradient(90deg, #f598a8, #f6edb2)">
                <div class="flex">
                    <div class="cart">
                        <div class="img">
                            <img src="https://img.icons8.com/ios-filled/50/000000/mac-os.png" />
                        </div>
                        <div class="description">
                            <p>Приложение</br> iOS</p>Мы разрабатываем и поддерживаем приложения для iOS
                        </div>
                        <div class="price">2300 руб./час</div>
                        <a href="#zatemnenie" class="button">Заказать</a>
                    </div>
                    <div class="cart">
                        <div class="img">
                            <img src="https://img.icons8.com/ios-filled/50/000000/android-os.png" />
                        </div>
                        <div class="description">
                            <p>Приложения Android</p>Приложения для смартфонов и планшетов на Android и даже Android TV
                        </div>
                        <div class="price">2300 руб./час</div>
                        <a href="#zatemnenie" class="button">Заказать</a>
                    </div>
                    <div class="cart">
                        <div class="img">
                            <img src="https://img.icons8.com/ios-filled/50/000000/windows-10.png" />
                        </div>
                        <div class="description">
                            <p>Приложения Windows</p>Мы разрабатываем и поддерживаем приложения для Windows
                        </div>
                        <div class="price">2300 руб./час</div>
                        <a href="#zatemnenie" class="button">Заказать</a>
                    </div>
                </div>
            </div>
            <div id="zatemnenie">
                <div class="okno">
                <input type="email" class="email-on" placeholder="Введите Ваш Email..."/>
                    <a href="#" class="button">Отослать</a>
                </div>
            </div>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
            </br>
    </section>
    <footer id="footer" class="footer">
        <div class="container">
            <div class="adress">
                <h3>
                    Адрес
                </h3>
                <p>
                    344000, г. <br> Ростов-на-Дону, ул. пер.Кировский 35/113
                </p>
            </div>
            <div class="social">
                <h3>
                    Соцсети
                </h3>
                <div class="soc-links">
                    <a href="#">
                        <img src="images/1.svg" alt="Фейсбук">
                    </a>
                    <a href="#">
                        <img src="images/2.svg" alt="Вконтакте">
                    </a>
                    <a href="#">
                        <img src="images/3.svg" alt="Твиттер">
                    </a>
                    <a href="#">
                        <img src="images/4.svg" alt="Инстаграм">
                    </a>
                    <a href="#">
                        <img src="images/5.svg" alt="Ютуб">
                    </a>
                    <a href="#">
                        <img src="images/6.svg" alt="Телеграм">
                    </a>
                </div>
            </div>
            <div class="email">
                <h3>
                    Почта
                </h3>
                <a href="mailto:v@webant.ru">
                    v@webant.ru
                </a>
            </div>
        </div>
    </footer>
    <style>
        #zatemnenie {
            background: rgba(102, 102, 102, 0.5);
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            left: 0;
            display: none;
        }

        .okno {
            width: 30%;
            height: 15%;
            text-align: center;
            padding: 15px;
            border: 3px solid #000000;
            border-radius: 10px;
            color: #000000;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            margin: auto;
            background: #fff;
        }

        #zatemnenie:target {
            display: block;
        }

        .close {
            display: inline-block;
            border: 1px solid #000000;
            color: #000000;
            padding: 0 25px;
            margin: 3px;
            text-decoration: none;
            background: #f2f2f2;
            font-size: 18pt;
            cursor: pointer;
        }

        .close:hover {
            background: #e6e6ff;
        }
    </style>

</html>