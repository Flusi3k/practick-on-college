<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="css/main.css"> <!-- подключаем файл стилей -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon">
    <title>Document</title>
</head>

<body background="images/bg-main.jpg">
    <header id="header" class="header">
        <div class="container">
            <div class="navigation">
                <a href="index.php"><img src="images/logo.png" alt="Webant" class="logo-Webant"></a>
                <ul class="menu">
                    <li>
                        <a href="index.php" id="glave" class="main">
                            Главная
                        </a>
                    </li>
                    <li>
                        <a href="portfolio.php" class="contacts">
                            Портфолио
                        </a>
                    </li>
                    <li>
                        <a href="services.php" class="faculties">
                            Услуги и стоимость
                        </a>
                    </li>
                    <li>
                        <a href="events.php" class="rules">
                            Мероприятия
                        </a>
                    </li>
                    <li>
                        <a href="/includes/auth/register.php" class="">
                            Регистрация
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <section id="content" class="content">
        <div class="container">
            <h2 id="glawe-title">Webant:</h2>
            <div id="main-content" class="offering" style="border: 3px solid transparent; border-image: 10 repeating-linear-gradient(90deg, #f598a8, #f6edb2)">
                <p><img src="images/big-logo.png" alt="big-logo" class="big-logo">
                    <br>
                    <br>
                <h2> О компании «WebAnt»</h2>
                ​Компания WebAnt специализируется на разработке программного обеспечения в области автоматизации бизнес-процессов коммерции.
                <br>
                <br>
                Компания осуществляет дизайн, разработку и интеграцию сервисов и приложений.
                <br>
                <br>
                <div id="text-main" class="of-left">
                    <p><img src="images/daron.png" alt="daron" class="daron">
                        Я Владимир — основатель и руководитель студии Вебант. Мы делаем
                        мобильные приложения, сайты и ещё много всего интересного.
                        Работаем больше 10-ти лет, так что с опытом у нас всё в порядке
                        — через нас прошли сотни проектов.
                        <br>
                        <br>
                        Новые проекты я всегда обсуждаю с заказчиком лично. Вникаю в
                        бизнес-задачи, предлагаю оптимальное решение и формирую команду.
                        Если по каким-то причинам мы вам не сможем помочь, я вам честно
                        об этом скажу.
                        <br>
                        <br>
                        Пишите — обсудим ваш проект. Как минимум, помогу советом. А
                        может мы — это именно те, кого не хватало вашему проекту для
                        взрывного роста 😉
                    <h5>Владимир Даронь</h5>
                    <h6>Директор студии webant</h6>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <footer id="footer" class="footer">
        <div class="container">
            <div class="adress">
                <h3>
                    Адрес
                </h3>
                <p>
                    344000, г. <br> Ростов-на-Дону, пер.Кировский 35/113
                </p>
            </div>
            <div class="social">
                <h3>
                    Соцсети
                </h3>
                <div class="soc-links">
                    <a href="#">
                        <img src="images/1.svg" alt="Фейсбук">
                    </a>
                    <a href="#">
                        <img src="images/2.svg" alt="Вконтакте">
                    </a>
                    <a href="#">
                        <img src="images/3.svg" alt="Твиттер">
                    </a>
                    <a href="#">
                        <img src="images/4.svg" alt="Инстаграм">
                    </a>
                    <a href="#">
                        <img src="images/5.svg" alt="Ютуб">
                    </a>
                    <a href="#">
                        <img src="images/6.svg" alt="Телеграм">
                    </a>
                </div>
            </div>
            <div class="email">
                <h3>
                    Почта
                </h3>
                <a href="mailto:v@webant.ru">
                    v@webant.ru
                </a>
            </div>
        </div>
    </footer>
</body>

</html>